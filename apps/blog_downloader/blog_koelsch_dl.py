import os
import re

from apps.blog_downloader.downloader_classes import KoelschManager
from _constant_folders import KOELSCH_HTML, KOELSCH_RAW_TXT

manager = KoelschManager()
manager.get_articles_urls()
manager.get_html_from_urls()

# Takes each downloaded article, gets its content and transforms it in a clean txt file
with os.scandir(KOELSCH_HTML + "op-koelsch-verzallt") as iterator:
    # Création du dossier
    os.makedirs(KOELSCH_RAW_TXT + manager.blog_name, exist_ok=True)
    for entry in iterator:
        match_id = re.search(r"(?P<id>\d+)", entry.name)
        article_id = match_id.group('id')
        with open(entry, 'r', encoding='utf-8') as article:
            article_content = article.read()
            title, content = manager.clean_html(article_content)
        # Removing characters that are forbidden in a filename : #!^$()\[]{}?+*.<>':"/
        cleaned_title = re.sub(r"[#!^$()\[\]{}?+*.<>':\"/\\ ]|\s", "_", title)
        cleaned_file_path = "{}{}/K{}-{}.txt".format(KOELSCH_RAW_TXT, manager.blog_name,
                                                     article_id, cleaned_title[0:50])
        with open(cleaned_file_path, "w", encoding='utf-8') as cleaned_f:
            cleaned_f.write(content)
            print("Nouvel article créé : {} (id = {})".format(title, article_id))
