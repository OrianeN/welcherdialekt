from apps.blog_downloader.downloader_classes import AlemannischManager, scrap_source
from _constant_folders import ALS_URL_LIST, ALS_HTML, ALS_RAW_TXT

manager = AlemannischManager()
manager.get_article_urls(ALS_URL_LIST)

scrap_source(manager, ALS_HTML, ALS_RAW_TXT, "A")
