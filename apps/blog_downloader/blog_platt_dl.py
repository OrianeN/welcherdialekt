import os
import re

from apps.blog_downloader.downloader_classes import PlattManager
from _constant_folders import PLATT_HTML, PLATT_RAW_TXT

manager = PlattManager()
# manager.get_articles_urls()
# manager.get_html_from_urls(encoding="iso-8859-1")

# Takes each downloaded article, gets its content and transforms it in a clean txt file
with os.scandir(PLATT_HTML + "De-Plattsnackers") as iterator:
    for entry in iterator:
        match_id = re.search(r"(?P<id>\d+)", entry.name)
        article_id = match_id.group('id')
        with open(entry, 'r', encoding='iso-8859-1') as article:
            article_content = article.read()
            title, content = manager.clean_html(article_content)
        # Removing characters that are forbidden in a filename : #!^$()\[]{}?+*.<>':"/
        cleaned_title = re.sub(r"[#!^$()\[\]{}?+*.<>':\"/\\ ]|\s", "_", title)
        cleaned_file_path = "{}{}/Platt{}-{}.txt".format(PLATT_RAW_TXT, manager.blog_name,
                                                         article_id, cleaned_title[0:50])
        with open(cleaned_file_path, "w", encoding='utf-8') as cleaned_f:
            cleaned_f.write(content)
            print("Nouvel article créé : {} (id = {})".format(title, article_id))
