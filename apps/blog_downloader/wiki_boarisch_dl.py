from apps.blog_downloader.downloader_classes import BoarischManager, scrap_source
from _constant_folders import BOA_URL_LIST, BOA_HTML, BOA_RAW_TXT

manager = BoarischManager()
manager.get_article_urls(BOA_URL_LIST)

scrap_source(manager, BOA_HTML, BOA_RAW_TXT, "BOA")
