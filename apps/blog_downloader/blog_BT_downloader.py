import os
import re

from apps.blog_downloader.downloader_classes import BerlinTypischManager
from _constant_folders import BERLIN_HTML, BERLIN_RAW_TXT

manager = BerlinTypischManager()
manager.get_articles_urls()
manager.get_html_from_urls()

# Takes each downloaded article, gets its content and transforms it in a clean txt file
article_id = 0
with os.scandir(BERLIN_HTML + "BerlinTypisch") as iterator:
    for entry in iterator:
        with open(entry, 'r', encoding='utf-8') as article:
            article_content = article.read()
            title, content = manager.clean_html(article_content)
        # Removing characters that are forbidden in a filename : #!^$()\[]{}?+*.<>':"/
        cleaned_title = re.sub("[#!^$()\[\]{}?+*.<>':\"/]", "_", title)
        cleaned_file_path = "{}{}/BT{}-{}.txt".format(BERLIN_RAW_TXT, manager.blog_name,
                                                      article_id, cleaned_title[0:50])
        with open(cleaned_file_path, "w", encoding='utf-8') as cleaned_f:
            article_id += 1
            cleaned_f.write(title + "\n")
            cleaned_f.write(content)
            print("Nouvel article créé : {} (id = {})".format(title, article_id))
