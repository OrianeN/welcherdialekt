import os

ROOT = os.path.dirname(__file__)

BERLINERISCH = ROOT + "/Corpus/Berlinerisch/"
BERLIN_HTML = BERLINERISCH + "html_articles/"
BERLIN_RAW_TXT = BERLINERISCH + "raw_txt_articles/"

PLATT = ROOT + '/Corpus/Platt/'
PLATT_HTML = PLATT + "html_articles/"
PLATT_RAW_TXT = PLATT + "raw_txt_articles/"

KOELSCH = ROOT + "/Corpus/Koelsch/"
KOELSCH_HTML = KOELSCH + "html_articles/"
KOELSCH_RAW_TXT = KOELSCH + "raw_txt_articles/"

ALS = ROOT + '/Corpus/Alemannisch/'
ALS_URL_LIST = ALS + "urls.txt"
ALS_HTML = ALS + 'html_articles/'
ALS_RAW_TXT = ALS + "raw_txt_articles/"

BOARISCH = ROOT + '/Corpus/Boarisch/'
BOA_URL_LIST = BOARISCH + "urls.txt"
BOA_HTML = BOARISCH + 'html_articles/'
BOA_RAW_TXT = BOARISCH + "raw_txt_articles/"
