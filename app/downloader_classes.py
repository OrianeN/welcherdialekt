import os
import requests
import re

from bs4 import BeautifulSoup, NavigableString, Tag

from _constant_folders import BERLIN_HTML, PLATT_HTML, KOELSCH_HTML, ALS_HTML, BOA_HTML


def scrap_source(source_manager, html_path, txt_path, abr):
    """
    Function for scraping articles from a given source
    :param source_manager: instance object of a class under BlogDownloader (ex: instance of BoarischManager)
    :param html_path: string path to the folder where the HTML articles have been downloaded
    :param txt_path: string path to the folder where the final TXT files will be saved
    :param abr: string abreviation of the source, to put with the article's ID
    :return: None, but saves the articles in TXT format
    """
    source_manager.get_html_from_urls()
    # Prendre chaque article téléchargé, récupérer le contenu et le transformer en txt cleaned
    with os.scandir(html_path + source_manager.blog_name) as iterator:
        # Création du dossier
        os.makedirs(txt_path + source_manager.blog_name, exist_ok=True)
        for entry in iterator:
            match_id = re.search(r"(?P<id>\d+)", entry.name)
            article_id = match_id.group('id')
            with open(entry, 'r', encoding='utf-8') as article:
                article_content = article.read()
                title, content = source_manager.clean_html(article_content)
            # On enlève les caractères interdits pour la création du chemin du fichier final : #!^$()\[]{}?+*.<>':"/
            cleaned_title = re.sub(r"[#!^$()\[\]{}?+*.<>':\"/\\ ]|\s", "_", title)
            cleaned_file_path = "{}{}/{}{}-{}.txt".format(txt_path, source_manager.blog_name,
                                                           abr, article_id, cleaned_title[0:50])
            with open(cleaned_file_path, "w", encoding='utf-8') as cleaned_f:
                cleaned_f.write(content)
                print("Nouvel article créé : {} (id = {})".format(title, article_id))


def get_url_content(url):
    """Function that gets html content from a url"""
    url_content = None
    try:
        r = requests.get(url)
    except Exception as e:
        print("Error while getting the content of {} : {}".format(url, e))
    else:
        if r.status_code == 200:
            url_content = r.text
    return url_content


class ArticlesFetcherFromUrls:
    """This class takes a list of urls, downloads their html content and makes an output in the given folder"""
    def __init__(self, urls_list):
        """Constructor"""
        self.urls = urls_list

    def download(self, output_folder, encoding):
        """Main class"""
        count = 1
        total = len(self.urls)
        for url in self.urls:
            url_content = get_url_content(url)
            if url_content is None:
                continue
            base_name = "article" + str(count)
            article_filename = base_name + ".html"

            # Création du dossier
            os.makedirs(output_folder, exist_ok=True)

            with open(output_folder + article_filename, 'w', encoding=encoding) as pf:
                pf.write(url_content)
            print('{}/{} - {} was downloaded successfully'.format(count, total, article_filename))
            count += 1
        print('A total of {} urls were downloaded'.format(count-1))
        return 200


class BlogDownloader:
    """Class used for the automatic scrapping of articles from URLs"""
    beginning_year = 1993

    def __init__(self):
        """Constructor"""
        self.articles_urls = []
        self.output_folder = ""

    def get_html_from_urls(self, encoding="utf-8"):
        """This method creates the html output for all urls found on the websites"""
        output_folder = self.output_folder
        print("Création des fichiers HTML")
        article_fetcher = ArticlesFetcherFromUrls(self.articles_urls)
        response = article_fetcher.download(output_folder, encoding)
        return response

    def clean_and_print_urls(self):
        print("{} urls ont été trouvées !".format(len(self.articles_urls)))
        self.articles_urls = list(set(self.articles_urls))  # On supprime les doublons
        self.articles_urls.sort()  # On trie la liste par ordre alphabétique
        print("{} url différentes ont été trouvées !".format(len(self.articles_urls)))  # On affiche le nombre d'url


class BerlinTypischManager(BlogDownloader):
    """Downloader for the extraction of articles from the main page of BerlinTypisch blog."""

    def __init__(self):
        """Constructor"""
        BlogDownloader.__init__(self)
        self.blog_name = "BerlinTypisch"
        self.blog_url = "https://berlintypisch.wordpress.com/"
        self.output_folder = BERLIN_HTML + self.blog_name + "/"

    def get_articles_urls(self):
        """This method gets a maximum of urls that will then be downloaded"""
        print("Récupération des urls de {}".format(self.blog_name))
        # Récupérer le contenu de chaque page
        page_nb = 0
        while page_nb < 31:
            page_nb += 1
            print("Récupération des urls, page {}. {} urls ont déjà été trouvées".format(page_nb, len(self.articles_urls)))
            page_content = get_url_content(self.blog_url + "page/{}/".format(page_nb))
            page_tree = BeautifulSoup(page_content, "lxml")
            # Récupérer la div contenant les articles
            content = page_tree.find("div", id="content")
            entries = content.find_all("h2", class_="entry-title")
            # Récupérer les urls des articles
            for entry in entries:
                article_url = entry.a.get("href")
                self.articles_urls.append(article_url)
        BlogDownloader.clean_and_print_urls(self)

    def clean_html(self, file_content):
        """This methods creates a clean txt file out of an html article from the blog"""
        page_tree = BeautifulSoup(file_content, "lxml")
        title = page_tree.find("h1", class_="entry-title").get_text()
        content = ""
        content_part = page_tree.find("div", class_="entry-content")
        for subpart in content_part.children:
            if subpart.name == "div":
                if subpart.has_attr('id'):
                    if re.compile("atatags").search(subpart['id']):
                        break
                else:
                    continue
            elif isinstance(subpart, NavigableString):
                continue
            else:
                content += subpart.get_text() + "\n"
        return title, content


class PlattManager(BlogDownloader):
    """Downloader for the extraction of articles from the website
    http://www.de-plattsnackers.de/."""

    def __init__(self):
        """Constructor"""
        BlogDownloader.__init__(self)
        self.blog_name = "De-Plattsnackers"
        self.blog_url = "http://www.de-plattsnackers.de/cms/Inhalt-zeigeGeschichten/"
        self.url_basis = "http://www.de-plattsnackers.de"
        self.output_folder = PLATT_HTML + self.blog_name + '/'

    def get_articles_urls(self):
        """This method gets a maximum of urls that will then be downloaded
        :return: None, but adds the URLS in the array self.articles_urls"""
        print("Récupération des urls de {}".format(self.blog_name))
        # On télécharge la page comprenant la liste des articles
        page_content = get_url_content(self.blog_url)
        page_tree = BeautifulSoup(page_content, "lxml")
        # On récupère les URLS des articles
        menu_and_list = page_tree.find("div", align="center").find("table").\
            find("table").find("table")
        articles_list = menu_and_list.find("table").find_next("table").\
            find_next("table")
        url_tags = articles_list.find_all("a")
        for entry in url_tags:
            article_url = entry.get("href")
            self.articles_urls.append(self.url_basis + article_url)
        BlogDownloader.clean_and_print_urls(self)


    def clean_html(self, file_content):
        """This methods creates a clean txt file out of an html article from the blog
        :param file_content: str
        :return: str (title), str (article content)"""
        page_tree = BeautifulSoup(file_content, "lxml")
        menu_and_article = page_tree.find("div", align="center").find("table").\
            find("table").find("table")
        article_section = menu_and_article.find("table").find_next("table").\
            find_next("table")
        # Récupérer le titre
        title = "untitled"
        title_tag = article_section.find("h1")
        if title_tag:
            title = title_tag.get_text()
        # On récupère le texte en rajoutant des retours à la ligne
        content = ""
        line = 1
        for string in article_section.stripped_strings:
            if line == 1:
                content += string + "\n"
                if not title_tag:
                    title = string
            else:
                content += string + " "
                match_ending = re.search(r"[?.!]$", string)
                if match_ending:
                    content += "\n"
            line += 1
        return title, content


class KoelschManager(BlogDownloader):
    """Downloader for the extraction of articles from the website https://www.heimatverein-alt-koeln.de."""

    def __init__(self):
        """Constructor"""
        BlogDownloader.__init__(self)
        self.blog_name = "op-koelsch-verzallt"
        self.url_basis = "https://www.heimatverein-alt-koeln.de/op-koelsch-verzallt/"
        self.blog_url_a = self.url_basis + "texte-unserer-mitglieder/"
        self.blog_url_b = self.url_basis + "texte-aus-dem-mittwochskreis/"
        self.output_folder = KOELSCH_HTML + self.blog_name + '/'

    def get_articles_urls(self):
        """This method gets a maximum of urls that will then be downloaded"""
        print("Récupération des urls de {}".format(self.blog_name))
        page_nb = 0
        for page_url in [self.blog_url_a, self.blog_url_b]:
            page_nb += 1
            page_content = get_url_content(page_url)
            page_tree = BeautifulSoup(page_content, "lxml")
            # On récupère les URLs des articles
            page_article_section = page_tree.article.find("div", class_="entry-content")
            url_tags = page_article_section.find_all("a")
            link_nb = 0
            for entry in url_tags:
                article_url = entry.get("href")
                link_nb += 1
                if link_nb == 1 and page_nb == 2:
                    continue
                else:
                    self.articles_urls.append(article_url)
        BlogDownloader.clean_and_print_urls(self)


    def clean_html(self, file_content):
        """This methods creates a clean txt file out of an html article from the blog"""
        page_tree = BeautifulSoup(file_content, "lxml")
        article = page_tree.article.find("div", class_="entry-content")
        # Récupérer le titre
        title = article.p.get_text()
        # Récupérer le contenu de l'article
        content = ""
        paragraphs = article.find_all("p")
        for paragraph in paragraphs:
            for string in paragraph.stripped_strings:
                content += string + " "
            content += "\n"
        return title, content


class AlemannischManager(BlogDownloader):
    """Downloader for the extraction of articles from the Alemannisch Wikipedia"""

    def __init__(self):
        BlogDownloader.__init__(self)
        self.blog_name = "Alemannisch Wikipedia"
        self.output_folder = ALS_HTML + self.blog_name + '/'


    def get_article_urls(self, file_path):
        """Fetches the articles URLs listed in the given file"""
        with open(file_path, "r", encoding="utf-8") as f:
            lines = f.readlines()
            for line in lines:
                url = line.rstrip()
                if url:
                    self.articles_urls.append(url)
        BlogDownloader.clean_and_print_urls(self)


    def clean_html(self, file_content):
        """This methods creates a clean txt file out of an html article from the Alemannisch Wiki"""
        content = ""
        page_tree = BeautifulSoup(file_content, "lxml")
        article_section = page_tree.body.find("div", id="content")
        title = article_section.find("h1", id="firstHeading").get_text()
        text_section = article_section.find("div", id="mw-content-text").div
        for child_tag in text_section.children:
            if isinstance(child_tag, Tag) and (child_tag.name == "p" or child_tag.name.startswith('h')):
                for string in child_tag.stripped_strings:
                    content += string + " "
                content += "\n"
        return title, content


class BoarischManager(BlogDownloader):
    """Downloader for the extraction of articles from the Boarisch Wikipedia"""

    def __init__(self):
        BlogDownloader.__init__(self)
        self.blog_name = "Boarisch Wikipedia"
        self.output_folder = BOA_HTML + self.blog_name + '/'


    def get_article_urls(self, file_path):
        """Fetches the articles URLs listed in the given file"""
        with open(file_path, "r", encoding="utf-8") as f:
            lines = f.readlines()
            for line in lines:
                url = line.rstrip()
                if url:
                    self.articles_urls.append(url)
        BlogDownloader.clean_and_print_urls(self)

    def clean_html(self, file_content):
        """This methods creates a clean txt file out of an html article from the Boarisch Wiki"""
        content = ""
        page_tree = BeautifulSoup(file_content, "lxml")
        article_section = page_tree.body.find("div", id="content")
        title = article_section.find("h1", id="firstHeading").get_text()
        text_section = article_section.find("div", id="mw-content-text").div
        for child_tag in text_section.children:
            if isinstance(child_tag, Tag) and (child_tag.name == "p" or
                                               child_tag.name.startswith('h') or
                                               child_tag.name == "dl"):
                for string in child_tag.stripped_strings:
                    content += string + " "
                content += "\n"
        return title, content

